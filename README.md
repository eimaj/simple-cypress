# Cypress e2e Testing Ground

This is a sandbox for learning the ways of Cypress.

## Dependencies

[Node](https://nodejs.org/en/download/)

Version doesn't really matter, but this project uses `v12.13.0` and I use `nvm` to manage my node verisons.

## Setup

```
$ git clone https://gitlab.com/eimaj/simple-cypress.git
$ cd simple-cypress
$ nvm use
$ npm i
```

### Config

Add a `baseUrl` to `cypress.json`. This will be the URL of the site you are looking to test.

This will work with `http://localhost:1234` as long as it is accessible from the location you are running Cypress.

## Run

Make sure you have the app running locally. These test look for the app on port `3000`.

```
$ npm run cypress
```

This will open up the Cypress app. Select the integration test you want to run from the list. Any changes you make to the test will trigger a re-run.

## Write tests

The only other thing there is to do here is add some tests to the `tests/integrations` folder.

Watch [this video (Writing your first test)](https://vimeo.com/237115455) to see how easy it is to get started.
